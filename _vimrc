" required
set nocompatible

set encoding=utf-8


" CTags. Go to project dir and run 'ctags -R'.
" Cursor on and press Ctrl-J to find definition
"set tags=tags


"disable error sound
set belloff=all

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'

"---------------------------------------
" Add all your plugins here
"---------------------------------------
"git interface
Plugin 'tpope/vim-fugitive'

" Statusbar
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
let g:airline_theme='minimalist'

"filesystem
Plugin 'scrooloose/nerdtree'
"Plugin 'jistr/vim-nerdtree-tabs'

"python sytax checker
"Plugin 'nvie/vim-flake8'
"Plugin 'vim-scripts/Pydiction'
"Plugin 'vim-scripts/indentpython.vim'
"Plugin 'scrooloose/syntastic'

"auto-completion stuff
"Plugin 'davidhalter/jedi-vim'
Plugin 'kien/ctrlp.vim'

"code folding
Plugin 'tmhedberg/SimpylFold'

"Git diff in gutter
Plugin 'airblade/vim-gitgutter'

"Colors!!!
Plugin 'dracula/vim'
Plugin 'drewtempelmeyer/palenight.vim'
Plugin 'altercation/vim-colors-solarized'
Plugin 'jnurmine/Zenburn'
Plugin 'cocopon/iceberg.vim'
Plugin 'whatyouhide/vim-gotham'
Plugin 'joshdick/onedark.vim'

" Quoting / paranthesizing
Plugin 'tpope/vim-surround'

" Comment out stuff (gcc or gc+movement)
Plugin 'tpope/vim-commentary'

"MEMO: manual commenting in/out code
"   Comment
"       ctrl-v
"       select lines
"       shift-I
"       #
"       esc esc
"
"   Uncomment
"       ctrl-v
"       select lines
"       d   or  r  or  x
"---------------------------------------
" All of your Plugins must be added before the following line
call vundle#end()            " required

filetype plugin indent on    " enables filetype detection
let g:SimpylFold_docstring_preview = 1

" Disable folding by default
set nofoldenable
" to activate folding type zA in normalmode

" Automatic reloading of .vimrc
"" autocmd! bufwritepost .vimrc source %

" Better copy & paste
" When you want to paste large blocks of code into vim, press F2 before you
" paste. At the bottom you should see ``-- INSERT (paste) --``.

"" set pastetoggle=<F2>
set clipboard=unnamed


" Mouse and backspace
"" set mouse=a  " on OSX press ALT and click
"" set bs=2     " make backspace behave like normal again


" Rebind <Leader> key
" I like to have it here becuase it is easier to reach than the default and
" it is next to ``m`` and ``n`` which I use for navigating between tabs.
let mapleader = " "


" Bind nohl
" Removes highlight of your last search
" ``<C>`` stands for ``CTRL`` and therefore ``<C-n>`` stands for ``CTRL+n``
"" noremap <C-n> :nohl<CR>
"" vnoremap <C-n> :nohl<CR>
"" inoremap <C-n> :nohl<CR>


" Quicksave command
"" noremap <C-Z> :update<CR>
"" vnoremap <C-Z> <C-C>:update<CR>
"" inoremap <C-Z> <C-O>:update<CR>


" Quick quit command
"" noremap <Leader>e :quit<CR>  " Quit current window
"" noremap <Leader>E :qa!<CR>   " Quit all windows


" bind Ctrl+<movement> keys to move around the windows, instead of using Ctrl+w + <movement>
" Every unnecessary keystroke that can be saved is good for your health :)
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h


" easier moving between tabs
map <Leader>n <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>

" map sort function to a key
vnoremap <Leader>s :sort<CR>

" easier moving of code blocks
" Try to go into visual mode (v), thenselect several lines of code here and
" then press ``>`` several times.
vnoremap < <gv  " better indentation
vnoremap > >gv  " better indentation

" Show whitespace
" MUST be inserted BEFORE the colorscheme command
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
au InsertLeave * match ExtraWhitespace /\s\+$/

" useful settings
set history=700
set undolevels=700

" Colors
set t_Co=256
colorscheme zenburn
colorscheme onedark

if has('gui_win32')
  set guifont=Ubuntu_Mono:h18:cANSI:qDRAFT
else
  set guifont=Ubuntu\ Mono\ 14
endif

" Disable stupid backup and swap files - they trigger too many events
" for file system watchers
"set nobackup
"set nowritebackup
set noswapfile

"turn on numbering
set nu

"More natural split opening
set splitbelow
set splitright

"Nerdtree
"Open automaticlly
autocmd vimenter * NERDTree
"Toggle nerdtree with shortcut
nnoremap <C-n> :NERDTreeToggle<return>
"open automaticlly when no file specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
"quit if only nerdtree is open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
"ToggleNERDTree when file is loaded
let NERDTreeQuitOnOpen = 1
"Make prettier
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
"Close vim if NERDTree is the only window left open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
"ignore files in NERDTree
let NERDTreeIgnore=['\.pyc$', '\~$']

" Settings for ctrlp
let g:ctrlp_max_height = 30
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=*/coverage/*

"------------Start Python PEP 8 stuff----------------
" Number of spaces that a pre-existing tab is equal to.
au BufRead,BufNewFile *py,*pyw,*.c,*.h set tabstop=4

"spaces for indents
au BufRead,BufNewFile *.py,*pyw set shiftwidth=4
au BufRead,BufNewFile *.py,*.pyw set expandtab
au BufRead,BufNewFile *.py set softtabstop=4

" Use the below highlight group when displaying bad whitespace is desired.
highlight BadWhitespace ctermbg=red guibg=red

" Display tabs at the beginning of a line in Python mode as bad.
au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/
" Make trailing whitespace be flagged as bad.
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" Wrap text after a certain number of characters
au BufRead,BufNewFile *.py,*.pyw, set textwidth=100

" Use UNIX (\n) line endings.
au BufNewFile *.py,*.pyw,*.c,*.h set fileformat=unix

" Set the default file encoding to UTF-8:
set encoding=utf-8

" For full syntax highlighting:
let python_highlight_all=1
syntax on
filetype off
filetype plugin indent on

" Red line to show max lenght of lines
"set colorcolumn=80
"highlight ColorColumn ctermbg=darkred

" Gray line to show max lenght of lines.
let &colorcolumn=join(range(81,999),",")
let &colorcolumn="80,".join(range(400,999),",")


au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2

" Keep indentation level from previous line:
autocmd FileType python set autoindent

" make backspaces more powerfull
set backspace=indent,eol,start

" System Clipboard
set clipboard=unnamed

" Make search case insensitive
set hlsearch
set incsearch
set ignorecase
set smartcase

"Folding based on indentation:
autocmd FileType python set foldmethod=indent

"Git gutter
let g:gitgutter_enabled = 1
map <F5> :GitGutterToggle<CR>
autocmd BufWritePost * GitGutter

"map# to execute current file in python3
map # :w<CR>:!py %<CR>

"use space to open folds
"nnoremap <space> za

"Show/hide invisible chars with: set list/set nolist
:set listchars=eol:$,tab:t>,trail:~,extends:>,precedes:<,space:.

"Syntastic recommended config
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

function! SyntasticCheckHook(errors)
    if !empty(a:errors)
        let g:syntastic_loc_list_height = min([len(a:errors), 10])
    endif
endfunction

silent! nmap <F6> :SyntasticToggleMode<CR>

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1

highlight Cursor guifg=white guibg=white

